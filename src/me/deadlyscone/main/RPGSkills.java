package me.deadlyscone.main;

import me.deadlyscone.events.player.PlayerChangeWorldHandler;
import me.deadlyscone.events.player.PlayerJoinHandler;
import me.deadlyscone.events.skills.MiningEventHandler;
import me.deadlyscone.interfaces.*;
import me.deadlyscone.operations.FileOperations;
import me.deadlyscone.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * Created by deadlyscone on 11/30/2015.
 */
public class RPGSkills extends JavaPlugin {

    private static boolean isHalting = false;
    public static boolean isDebugging = true;
    public static File pluginFolder;
    public ISkillConfigContainer skillConfigContainer;
    public IPluginConfiguration pluginConfiguration;
    public IUserRepository userRepository;

    //******************************************************************\
    //  On Enable
    //******************************************************************\
    @Override
    public void onEnable(){
        pluginFolder = this.getDataFolder();
        Console.showASCIIArt();
        // Run Update Check Async
        //UpdateManager.checkVersion(this);

        // Check Files
        FileOperations.VerifyOrCreateConfigs(this);
        this.userRepository = FileOperations.CreateOrLoadUserRepository();
        // Load Data
        this.pluginConfiguration = FileOperations.LoadPluginConfiguration(this);
        this.skillConfigContainer = FileOperations.LoadSkillConfigurations(this);

        // Register Events
        RegisterEvents();
    }

    //******************************************************************\
    //  On Disable
    //******************************************************************\
    @Override
    public void onDisable(){
        FileOperations.SaveUserRepository(this.userRepository);
    }

    //******************************************************************\
    //  Helper Functions
    //******************************************************************\
    private void RegisterEvents(){
        if(!isHalting){
            //  Listeners
            //new CommandRouter(this);
            new MiningEventHandler(this);
            new PlayerChangeWorldHandler(this);
            new PlayerJoinHandler(this);

            //  Commands
            //this.getCommand("template").setExecutor(new CommandRouter(this));
        }
    }

    public static void halt(Exception reason){
        isHalting = true;
        RPGSkills plugin = (RPGSkills) Bukkit.getServer().getPluginManager().getPlugin("RPGSkills");
        if(isDebugging){
            reason.printStackTrace();
        }else{
            //  Lets write it to an error log.
        }
        Console.severe("Shutting Down! | Reason: " + reason.getMessage());
        Bukkit.getServer().getPluginManager().disablePlugin(plugin);
    }
}
