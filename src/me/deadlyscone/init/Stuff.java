package me.deadlyscone.init;

import me.deadlyscone.main.RPGSkills;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by deadlyscone on 12/3/2015.
 */
public class Stuff {

    public static void writeWorldsToYml(File file, String YmlPath){
        FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(file);
        List<String> worldsToWrite = new ArrayList<String>();
        World main = Bukkit.getServer().getWorlds().get(0);
        worldsToWrite.add(main.getName()+",-");

        for(World w : Bukkit.getServer().getWorlds()){
            if (w != main){
                worldsToWrite.add(w.getName()+"," + main.getName());
            }
        }
        customSkillsConfig.set(YmlPath, worldsToWrite);
        try{
            customSkillsConfig.save(file);
        }catch(IOException e){
            RPGSkills.halt(new Exception("Error while writing to worlds to file."));
        }
    }
   /* private void writeConverterYMLWorlds(){
        FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(converter);
        String mainWorld = getServer().getWorlds().get(0).getName();
        List<String> thisList = new ArrayList<String>();
        thisList.add(mainWorld);
        try{
            customSkillsConfig.options().copyDefaults(true);
            customSkillsConfig.set("worlds", thisList);
            customSkillsConfig.save(converter);
        }catch(IOException e){
            System.out.println("[RPGSkills] Error while writing to converter.yml");
        }

    }*/

}
