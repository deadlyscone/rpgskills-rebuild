package me.deadlyscone.cmds;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.utils.Console;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deadlyscone on 11/21/2015.
 */
public class CommandRouter implements CommandExecutor{
    RPGSkills plugin;
    private AdminCommands adminCommands;
    private List<Boolean> Routes = new ArrayList<Boolean>();

    public CommandRouter(RPGSkills plugin){
        this.plugin = plugin;
        adminCommands = new AdminCommands(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        /*
              Routes to other command classes
         */
        final HookInfo hookInfo = new HookInfo(sender,cmd,label,args);
        boolean sendPluginHelp = true;

        Routes.add(adminCommands.createHook(hookInfo));

        for(boolean onCommandSuccess : Routes){ if(onCommandSuccess){ sendPluginHelp = false; break; } }
        if(sendPluginHelp){
            Console.showCommandHelp(sender, plugin);}
        Routes.clear();
        return true;
    }
}

/**
 *  ///////////////////////////////////////////////////////////////////////////////////////////
 *
 * 		Hook Info Object
 *
 ** //////////////////////////////////////////////////////////////////////////////////////////
 */

class HookInfo {
    private CommandSender sender;
    private Command cmd;
    private String label;
    private String[] args;

    public HookInfo(CommandSender sender, Command cmd, String label, String[] args){
        this.args = args;
        this.cmd = cmd;
        this.label = label;
        this.sender = sender;
    }

    public String getLabel() {
        return label;
    }

    public String[] getArgs() {
        return args;
    }

    public Command getCmd() {
        return cmd;
    }

    public CommandSender getSender() {
        return sender;
    }
}
