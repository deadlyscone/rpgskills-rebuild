package me.deadlyscone.containers;

import me.deadlyscone.interfaces.ISkillConfigContainer;
import me.deadlyscone.interfaces.skills.IMiningConfig;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Austin on 10/23/2016.
 */
public class SkillConfigContainer implements ISkillConfigContainer {

    private HashMap<UUID, IMiningConfig> miningConfigSettings;

    public SkillConfigContainer(){
        this.miningConfigSettings = new HashMap<>();
    }

    public void addMiningConfig(UUID worldUUID, IMiningConfig config){
        this.miningConfigSettings.put(worldUUID, config);
    }

    public IMiningConfig getMiningConfig(UUID worldUUID){
        return this.miningConfigSettings.get(worldUUID);
    }
}
