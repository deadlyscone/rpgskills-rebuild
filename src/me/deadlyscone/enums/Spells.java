package me.deadlyscone.enums;

/**
 * Created by deadlyscone on 11/30/2015.
 */
public abstract class Spells{

        public enum Type{
            Wind_Bolt, Snare, Bones_To_Apples, Fire_Bolt, Suffocate, Entangle, Shadow_Bolt, Regeneration, Super_Heat,

            Wind_Surge, Wind_Barrage, Bind, Eyeless, Confuse, Curse, Lightning_Bolt, Lightning_Barrage, Vulnerability,

            Humidify, Night_Vision, Gills, Vanish, Decay, Poison, Heal, Heal_Other, Heal_Group, Haste, Resist_Fire;
        }
    public enum Property{
        Damage, Push, Lift, Duration, Bones_Required, Fire_Ticks, Level, Material, Radius, Yield,

        Use_In_Non_PvP
    }
}