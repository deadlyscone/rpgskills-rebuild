package me.deadlyscone.enums;

/**
 * Created by deadlyscone on 12/3/2015.
 */
public enum BreedingMobType {
    Chicken, Cow, Horse, Mushroom_Cow, Ocelot, Pig, Rabbit, Wolf
}
