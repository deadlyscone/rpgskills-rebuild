package me.deadlyscone.enums;

/**
 * Created by deadlyscone on 11/30/2015.
 */
abstract class Farming {

    public enum Blocks{
        Crops, Melon_Block, Melon_Stem, Pumpkin_Stem, Cocoa, Carrot, Potato, Pumpkin,
                
        Sugar_Cane_Block, Nether_Warts, Cactus, Brown_Mushroom, Red_Mushroom
    }
}