package me.deadlyscone.enums;

/**
 * Created by deadlyscone on 5/28/2016.
 */
public enum Permissions {
    RPGSKILLS_SKILLS_MINING("rpgskills.skills.mining");

    private String value;

    Permissions(String value){
        this.value = value;
    }

    @Override
    public String toString(){
        return this.value.replace("_",".").toLowerCase();
    }
}
