package me.deadlyscone.enums;

import me.deadlyscone.utils.Console;
import org.bukkit.Material;
import org.bukkit.block.Block;

/**
 * Created by deadlyscone on 11/30/2015.
 */
public abstract class Mining {

    public final static String TOOL_SUFFIX = "_Pickaxe";

    public enum Blocks{
        Stone, Netherrack, Quartz_Ore, Coal_Ore, Iron_Ore, Gold_Ore, Redstone_Ore, Lapis_Ore, Emerald_Ore,
        Diamond_Ore;

        public static boolean isMiningBlock(Material material){
            for(Blocks block : Blocks.values()){
                String materialName = material.name();
                if(materialName.equalsIgnoreCase(block.toString()) || materialName.equalsIgnoreCase("GLOWING_REDSTONE_ORE")){
                    return true;
                }
            }
            return false;
        }
    }
}