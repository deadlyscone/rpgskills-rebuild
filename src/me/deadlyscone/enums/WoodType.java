package me.deadlyscone.enums;

/**
 * Created by deadlyscone on 12/3/2015.
 */
public enum WoodType {
    Oak, Birch, Spruce, Jungle, Acacia, Dark_Oak
}
