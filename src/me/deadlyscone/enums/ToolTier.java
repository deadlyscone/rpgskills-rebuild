package me.deadlyscone.enums;

/**
 * Created by deadlyscone on 12/3/2015.
 */

public enum ToolTier {
    Wood, Stone, Iron, Gold, Diamond
}