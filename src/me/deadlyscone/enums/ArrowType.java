package me.deadlyscone.enums;

/**
 * Created by deadlyscone on 11/30/2015.
 */
public enum ArrowType {
    Wood, Generic, Iron, Gold, Emerald, Diamond, Explosive, Frost, Elemental, Odisiac, Poison, Electric, Glass,
    Dragon, Enderbone
}
