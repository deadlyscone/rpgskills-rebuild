package me.deadlyscone.enums;

/**
 * Created by deadlyscone on 12/3/2015.
 */
public enum ArrowEffectProperty {
    Damage, Duration, Fire_Ticks, Magnitude, Level
}
