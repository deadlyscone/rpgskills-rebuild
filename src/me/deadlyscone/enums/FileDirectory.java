package me.deadlyscone.enums;

import me.deadlyscone.main.RPGSkills;

import java.io.File;

/**
 * Created by Austin on 10/22/2016.
 */
public class FileDirectory {
    public final static File pluginFolder = RPGSkills.pluginFolder;
    public final static File dataFolder = new File(pluginFolder, "/data");
    public final static File converterFile = new File(pluginFolder, "/converter.yml");
    public final static File usePermsFile = new File(pluginFolder, "/permissions.yml");
    public final static File configFile = new File(pluginFolder, "/config.yml");
    public final static File messagesFile = new File(pluginFolder, "/messages.yml");

    public final static File worldsFolder = new File(pluginFolder, "/worlds");
    public final static File worldsConfig = new File(pluginFolder, "/worlds/worlds.yml");
    public final static File antidropsConfig = new File(pluginFolder, "/antidrops.yml");
    public final static File recipesConfig = new File(pluginFolder, "/recipes.yml");
    public final static File ppb = new File(dataFolder, "/ppb.dat");
    public final static File pmp = new File(dataFolder, "/pmp.dat");
    public final static File mpu = new File(dataFolder, "/mpu.dat");
    public final static File mpb = new File(dataFolder, "/mpb.dat");
    public final static File userDat = new File(dataFolder, "/user.dat");
    public final static File repositoryDat = new File(dataFolder, "/repository.dat");
}
