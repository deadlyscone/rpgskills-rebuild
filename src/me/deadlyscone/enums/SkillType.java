package me.deadlyscone.enums;

/**
 * Created by deadlyscone on 11/30/2015.
 */
public enum SkillType {
    Attack, Archery, Defense, Excavation, Fishing, Fletching, Mining, Woodcutting, Magic, Farming, Unarmed,

    Breeding, Summoning, Eggcrafting
}