package me.deadlyscone.utils;

import me.deadlyscone.main.RPGSkills;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class ConfigParser {

	private enum ConfigFileType{RECIPES, MESSAGES, CONFIG}
	
	private static File dataFolder = Bukkit.getPluginManager().getPlugin("RPGSkills").getDataFolder();
	private static File worldsFolder = new File(dataFolder, "/worlds");
	private static List<String> listVar;
	private static int recursions = 0, errors = 0;
	private static boolean recur = false;
	
	public static void ParseAll(){
		if (recursions < 10){
			System.out.println("[RPGSkills] Parsing files [Pass " + (recursions+1) + "]");
			String lineIn = "";
			
			for (ConfigFileType file : ConfigFileType.values()){
				for (World world : Bukkit.getServer().getWorlds()){
					String currentFilePath = getExternalPath(file, world);
					URL resourceURL = getInternalPath(file);
					try {
						
			            FileReader fileReader = new FileReader(currentFilePath);
			            BufferedReader in = new BufferedReader(new InputStreamReader(resourceURL.openStream()));
			            BufferedReader ex = new BufferedReader(fileReader);
			            List<String>exListData = new ArrayList<String>();
			            List<String>inListData = new ArrayList<String>();
			            
			            
			            while((lineIn = in.readLine()) != null){
			            	inListData.add(lineIn);
			            }
			            while(exListData.size()<inListData.size()){
			            	exListData.add("");
			            }
			            exListData.addAll(0, Files.readAllLines(new File(currentFilePath).toPath()));
			            listVar =  new ArrayList<String>(getLists(inListData));
			            boolean hasError = false;
			            int ek = 0;
			            
			            for (int i = 0; i < inListData.size();i++){
			            	
			            		if (listVar.contains(exListData.get(ek))){
			            			
			            			ek++;
			            			while(i < exListData.size() && exListData.get(ek).trim().startsWith("-")){
			            				ek++;
			            			}
			            		}
			            		if (listVar.contains(inListData.get(i))){
			            			
			            			if (!exListData.contains(inListData.get(i))){
			            				errors++;
			            				hasError = true;
			            				i++;
			            				ek++;
			            				
			            			}else{
			            				i++;
				            			while(i < inListData.size() && inListData.get(i).trim().startsWith("-")){
				            				i++;
				            			}
			            			}
			            			
			            		}
			            		if(ek >= exListData.size() || i >= inListData.size()){
			            			break;
			            		}
			            		
			            		
			            	if (exListData.get(ek).equals(inListData.get(i))){
			            		ek++;
			            		continue;
			            	}else{
			            		if(inListData.get(i).contains(":") && !inListData.get(i).contains("#")){
					            	String[] exRaw = exListData.get(ek).split(":");
					            	String[] inRaw = inListData.get(i).split(":");
					            	if(!exRaw[0].equals(inRaw[0])){
					            		errors++;
					            		hasError = true;
					            	}
				            	}else{
				            		if (!exListData.get(ek).equals(inListData.get(i))){
				            			errors++;
					            		hasError = true;
				            		}
				            	}
			            	}
			            	ek++;
			            }
			            

			            if (hasError){
			            	
			            	for(int i = 0; i <inListData.size();i++){
			            		String s = inListData.get(i);
			            		
			            		if (!s.contains("#") && listVar.contains(s)){
			            			
			            			int index = exListData.indexOf(s);
			            			int il = i+1;
			            			if (index != -1){
			            				
			            				while (il < inListData.size() && !inListData.get(il).equals("") && inListData.get(il).trim().startsWith("-")){//removes all pieces
			            					inListData.remove(il);
			            				}
			            				
			            				int el = index+1;
			            				while (el < exListData.size() && !exListData.get(el).equals("") && exListData.get(el).trim().startsWith("-")){
			            					inListData.add((i+1), exListData.get(el));
			            					el++;
			            				}
			            			}
			            		}
			            		if(!s.contains("#") && s.split(":").length == 2){
			            			// if exListData does contain the value, use is value to ensure user defined properties stay the same
			            			
				            			String eS = s.split(":")[0].concat(":").concat(getIt(i, inListData, exListData).split(":")[1]);
				            			inListData.set(i, eS);
			            		}
			            		
			            	}
			            	
			            	Files.write(new File(currentFilePath).toPath(), inListData);
			            	 hasError = false;
			            	 recur = true;
			            	
			            }
			           
			            in.close();
			            ex.close();
			            
			        }catch(Exception e){
			        	e.printStackTrace();
			        }
				}
			}
			if (recur){
				System.out.println("[RPGSkills] Pass " + (recursions+1) + " found " + errors + " errors.");
				recur = false;
				recursions++;
				ParseAll();
			}else{
				System.out.println("[RPGSkills] 0 errors found.");
				System.out.println("[RPGSkills] Parsing files [DONE]");
			}
		}else{
			System.out.println("[RPGSkills] File parser was unable to resolve a configuration layout. Please manually check files serious for errors.");
		}
	}
	
	private static String getIt(int sIndex, List<String> inList, List<String> exList) {
		// return the exListData index as it relates to the inListData index
		
		for (int i = 0; i < exList.size(); i++) {
			String s = exList.get(i);

			if (!s.contains("#") && listVar.contains(s)) {

				int index = inList.indexOf(s);
    			int el = i+1;
    			if (index != -1){
    				
    				while (el < exList.size() && !exList.get(el).equals("") && exList.get(el).trim().startsWith("-")){//removes all pieces
    					exList.remove(el);
    				}
    				
    				int il = index+1;
    				while (il < inList.size() && !inList.get(il).equals("") && inList.get(il).trim().startsWith("-")){
    					exList.add((i+1), inList.get(il));
    					il++;
    				}
    			}
			}
		}

		
		for (int i = (exList.size()-1); i > -1; i--){
			
			if (!exList.get(i).equals("")){
				break;
			}
			exList.remove(i);
		}
		while (inList.size() > exList.size()){
			exList.add(0, "new");
		}
		//Both lists should be equal at this point:
		
			for(int t = sIndex; t < exList.size();t++){
				
				if (exList.get(t).split(":").length == 2 && inList.get(sIndex).split(":")[0].equals(exList.get(t).split(":")[0]) && 
						inList.get((sIndex-1)).split(":")[0].equals(exList.get((t-1)).split(":")[0])){
					return exList.get(t);
				}
			}
			return inList.get(sIndex);
	}
	
	private static List<String> getLists(List<String> list){
		List<String>readList = new ArrayList<String>();
		
		for (int i = 0; i<list.size();i++){
			
			if(list.get(i).trim().startsWith("-") && !list.get((i-1)).trim().startsWith("-") && !readList.contains(list.get((i-1)).trim())){
				String s = list.get((i-1)).trim();
				readList.add(s);
			}
		}
		return readList;
	}

	private static String getExternalPath(ConfigFileType file, World world) {
		switch(file){
		case CONFIG:
			return new File(dataFolder, "/" + file.toString().toLowerCase() + ".yml").toString();
		case MESSAGES:
			return new File(dataFolder, "/" + file.toString().toLowerCase() + ".yml").toString();
		case RECIPES:
			return new File(dataFolder, "/" + file.toString().toLowerCase() + ".yml").toString();
		default:
			return new File(worldsFolder, "/" + world.getName() + "/" + file.toString().toLowerCase() + ".yml").toString();
		}
	}
	
	private static URL getInternalPath(ConfigFileType file) {
		switch(file){
		case CONFIG:
			return RPGSkills.class.getResource("/"+ file.toString().toLowerCase() +".yml");
		case MESSAGES:
			return RPGSkills.class.getResource("/"+ file.toString().toLowerCase() +".yml");
		case RECIPES:
			return RPGSkills.class.getResource("/"+ file.toString().toLowerCase() +".yml");
		default:
			return RPGSkills.class.getResource("/skills/"+ file.toString().toLowerCase() +".yml");
		}
	}

}
