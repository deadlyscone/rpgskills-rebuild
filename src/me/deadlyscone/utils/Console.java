package me.deadlyscone.utils;

import me.deadlyscone.enums.ConsoleColor;
import me.deadlyscone.main.RPGSkills;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by deadlyscone on 11/8/2015.
 *
 * This class is meant to handle all logger formatting.
 */
public class Console {

    private static final String pluginName = "[RPGSkills] ";


    /**
     *  ///////////////////////////////////////////////////////////////////////////////////////////
     *
     * 		Console Messages
     *
     ** //////////////////////////////////////////////////////////////////////////////////////////
     */
    public static void info(Object msg){
        System.out.println(pluginName + msg);
    }
    public static void debug(Object msg) {
        if(RPGSkills.isDebugging)
        System.out.println(ConsoleColor.GREEN + pluginName + msg + ConsoleColor.RESET);
    }
    public static void severe(Object msg){
        System.out.println(ConsoleColor.LIGHT_RED + pluginName + msg + ConsoleColor.RESET);
    }
    public static void warning(Object msg){
        System.out.println(ConsoleColor.YELLOW + pluginName + msg + ConsoleColor.RESET);
    }

    public static void log(Object msg){
        System.out.println(ConsoleColor.DARK_CYAN + pluginName + msg + ConsoleColor.RESET);
    }

    /**
     *  ///////////////////////////////////////////////////////////////////////////////////////////
     *
     * 		Player Messages
     *
     ** //////////////////////////////////////////////////////////////////////////////////////////
     */
    public static void sendPlayerError(Player player, String message){
        player.sendMessage(ChatColor.RED + pluginName + " " + message);
    }
    public static void sendPlayerSuccess(Player player, String message){
        player.sendMessage(ChatColor.GREEN + pluginName + " " + message);
    }
    public static void sendPlayerWarning(Player player, String message){
        player.sendMessage(ChatColor.GOLD + pluginName + " " + message);
    }
    public static void sendPlayerInfo(Player player, String message){
        player.sendMessage(ChatColor.GRAY + pluginName + " " + message);
    }

    /**
     *  ///////////////////////////////////////////////////////////////////////////////////////////
     *
     * 		Other Messages
     *
     ** //////////////////////////////////////////////////////////////////////////////////////////
     */

    public static void showASCIIArt() {
        System.out.println("\n" +
                " ___ ___  __   __  _  ___ _   _    __  \n" +
                "| _ \\ _,\\/ _]/' _/| |/ / | | | | /' _/ \n" +
                "| v / v_/ [/\\`._`.|   <| | |_| |_`._`. \n" +
                "|_|_\\_|  \\__/|___/|_|\\_\\_|___|___|___/ \n"
        );


    }
    public static void showCommandHelp(CommandSender sender, RPGSkills plugin){
        if(sender instanceof Player){
            Player player = ((Player)sender);
            String authors = "";
            for(String auth : plugin.getDescription().getAuthors()){authors += (auth + ", ");}
            player.sendMessage(ChatColor.DARK_AQUA+"_________________["+ plugin.getName() +"]_________________");
            player.sendMessage(ChatColor.AQUA + "Version: " + plugin.getDescription().getVersion());
            player.sendMessage(ChatColor.AQUA + "Created by: " + authors);
            player.sendMessage(ChatColor.AQUA + "______________________________________________");
            if (sender.hasPermission("admin.commands")){
                player.sendMessage(ChatColor.GOLD + "---------------[Commands]---------------");
                player.sendMessage(ChatColor.RED + "/template reload  -  " + ChatColor.GREEN + "Reloads the plugin data and config.");
            }
        }else{
            System.out.println("["+ plugin.getName() +"] Commands: /template reload");
        }
    }

}
