package me.deadlyscone.operations;

import me.deadlyscone.containers.SkillConfigContainer;
import me.deadlyscone.enums.FileDirectory;
import me.deadlyscone.enums.SkillType;
import me.deadlyscone.init.Stuff;
import me.deadlyscone.interfaces.IPluginConfiguration;
import me.deadlyscone.interfaces.IUserRepository;
import me.deadlyscone.interfaces.skills.IMiningConfig;
import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.configs.MiningConfig;
import me.deadlyscone.obj.ParentChildWorldSet;
import me.deadlyscone.configs.PluginConfiguration;
import me.deadlyscone.repositories.UserRepository;
import me.deadlyscone.utils.Console;
import me.deadlyscone.utils.HelperUtil;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * Created by deadlyscone on 11/30/2015.
 */
public class FileOperations {

    public static PluginConfiguration LoadPluginConfiguration(RPGSkills plugin){
        PluginConfiguration config = new PluginConfiguration(plugin);
        return config;
    }

    public static SkillConfigContainer LoadSkillConfigurations(RPGSkills plugin){
        SkillConfigContainer container = new SkillConfigContainer();
        List<ParentChildWorldSet> loneChildWorlds = new ArrayList<>();
        List<ParentChildWorldSet> childWithParentWorlds = new ArrayList<>();
        List<ParentChildWorldSet> orderedWorlds = new ArrayList<>();
        Iterator it = plugin.pluginConfiguration.GetActiveWorlds().entrySet().iterator();
        while (it.hasNext()){
            Map.Entry<UUID, ParentChildWorldSet> pair = (Map.Entry<UUID, ParentChildWorldSet>)it.next();
            ParentChildWorldSet parentChildWorldSet = pair.getValue();
            if(!parentChildWorldSet.hasParentWorld()){
                loneChildWorlds.add(parentChildWorldSet);
            } else{
                childWithParentWorlds.add(parentChildWorldSet);
            }
        }
        orderedWorlds.addAll(0, loneChildWorlds);
        orderedWorlds.addAll((orderedWorlds.size()-1), childWithParentWorlds);

        for(ParentChildWorldSet parentChildWorldSet : orderedWorlds){
            UUID childWorldUUID = parentChildWorldSet.getChildWorld();
            if(!parentChildWorldSet.hasParentWorld()){
                String worldName = plugin.getServer().getWorld(childWorldUUID).getName();
                for(SkillType skillType : SkillType.values()){
                    FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(
                            new File(FileDirectory.worldsFolder + "/" + worldName + "/"+skillType.toString()+".yml")
                    );
                    switch (skillType){
                        case Attack:
                            break;
                        case Archery:
                            break;
                        case Defense:
                            break;
                        case Excavation:
                            break;
                        case Fishing:
                            break;
                        case Fletching:
                            break;
                        case Mining:
                            container.addMiningConfig(childWorldUUID, new MiningConfig(fileConfig, childWorldUUID));
                            break;
                        case Woodcutting:
                            break;
                        case Magic:
                            break;
                        case Farming:
                            break;
                        case Unarmed:
                            break;
                        case Breeding:
                            break;
                        case Summoning:
                            break;
                        case Eggcrafting:
                            break;
                    }
                }

            }else{
                IMiningConfig parentConfig = container.getMiningConfig(parentChildWorldSet.getParentWorld());
                container.addMiningConfig(childWorldUUID, parentConfig);
            }
        }
        return container;
    }

    public static UserRepository CreateOrLoadUserRepository(){
        IUserRepository userRepository = null;
        try{
            FileInputStream fis = new FileInputStream(FileDirectory.repositoryDat);
            ObjectInputStream ois = new ObjectInputStream(fis);
            userRepository = (IUserRepository)ois.readObject();
            ois.close();
        }catch(EOFException e){
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userRepository == null ? new UserRepository() : (UserRepository)userRepository;
    }

    public static void SaveUserRepository(IUserRepository userRepository){
        Console.log("Saving...");
        ObjectOutputStream oos;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(FileDirectory.repositoryDat, false));
            oos.writeObject(userRepository);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void VerifyOrCreateConfigs(RPGSkills plugin){
        //******************************************************************\
        //  File Variables and Directories
        //******************************************************************\
        Console.info("Checking Files [...]");
        if(!FileDirectory.pluginFolder.exists()){
            FileDirectory.pluginFolder.mkdir();
            Console.info("Created Plugin Folder!");
        }
        if (FileDirectory.antidropsConfig.exists()){
            FileDirectory.antidropsConfig.delete();
        }
        if (FileDirectory.usePermsFile.exists()){
            FileDirectory.usePermsFile.delete();
        }
        if(!FileDirectory.dataFolder.exists()){
            FileDirectory.dataFolder.mkdir();
        }
        if(!FileDirectory.worldsFolder.exists()){
            FileDirectory.worldsFolder.mkdir();
            Console.warning("Created Worlds Folder!");
        }
        if (!FileDirectory.userDat.exists()){
            try{
                FileDirectory.userDat.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        if (!FileDirectory.repositoryDat.exists()){
            try{
                FileDirectory.repositoryDat.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        if (!FileDirectory.configFile.exists()){
           HelperUtil.exportResource(RPGSkills.class.getResource("/config.yml"), FileDirectory.configFile);
        }
        if (!FileDirectory.messagesFile.exists()){
            HelperUtil.exportResource(RPGSkills.class.getResource("/messages.yml"), FileDirectory.messagesFile);
        }
        if (!FileDirectory.converterFile.exists()){
            HelperUtil.exportResource(RPGSkills.class.getResource("/converter.yml"), FileDirectory.converterFile);
            Stuff.writeWorldsToYml(FileDirectory.converterFile, "worlds");
        }
        if (!FileDirectory.recipesConfig.exists()){
            HelperUtil.exportResource(RPGSkills.class.getResource("/recipes.yml"), FileDirectory.recipesConfig);
        }
        if (!FileDirectory.worldsConfig.exists()){
            HelperUtil.exportResource(RPGSkills.class.getResource("/worlds/worlds.yml"), FileDirectory.worldsConfig);
            Stuff.writeWorldsToYml(FileDirectory.worldsConfig, "Active Worlds");
        }
        if (!FileDirectory.ppb.exists()){
            try{
                FileDirectory.ppb.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        if (!FileDirectory.pmp.exists()){
            try{
                FileDirectory.pmp.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        if (!FileDirectory.mpb.exists()){
            try{
                FileDirectory.mpb.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        if (!FileDirectory.mpu.exists()){
            try{
                FileDirectory.mpu.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        //******************************************************************\
        //  Creates per world files/dirs
        //******************************************************************\
        for(World world : plugin.getServer().getWorlds()){
            File currentWorld = new File(FileDirectory.worldsFolder, "/" + world.getName());
            if (!currentWorld.exists()) {
                currentWorld.mkdir();
            }
            for (SkillType skill : SkillType.values()){
                File skillYML = new File(plugin.getDataFolder(), "/worlds/" + world.getName() + "/" + skill.toString() +".yml");

                if (!skillYML.exists()){
                    URL resourceURL = RPGSkills.class.getResource("/skills/"+ skill.toString().toLowerCase() +".yml");
                    HelperUtil.exportResource(resourceURL, skillYML);
                }
            }
        }
        Console.info("Checking Files [DONE]");
    }
}


