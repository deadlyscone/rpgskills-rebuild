package me.deadlyscone.configs;

import me.deadlyscone.enums.FileDirectory;
import me.deadlyscone.enums.Spells;
import me.deadlyscone.interfaces.IPluginConfiguration;
import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.obj.ParentChildWorldSet;
import me.deadlyscone.utils.Console;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by deadlyscone on 5/29/2016.
 */
public class PluginConfiguration implements IPluginConfiguration {
    private RPGSkills plugin;

    private boolean isUsingSkillPerms, isUsingAntiDrops, isUsingPlayerListNames,
            isUsingJoinLeaveMessages, isSkillPanedAllowed;
    private HashMap<Spells.Type, String> spellbookLayoutMap;
    private ItemStack magicWandItemId;
    private HashMap<UUID, ParentChildWorldSet> activeWorlds;

    public PluginConfiguration(RPGSkills plugin){
        this.plugin = plugin;
        this.activeWorlds = new HashMap<>();
        this.spellbookLayoutMap = new HashMap<>();
        this.LoadActiveWorlds();
        this.LoadGlobalConfiguration();
    }

    @Override
    public void SetIsUsingSkillPermissions(boolean val) {
        this.isUsingSkillPerms = val;
    }

    @Override
    public boolean IsUsingSkillPermissions() {
        return this.isUsingSkillPerms;
    }

    @Override
    public void SetIsUsingAntiDrops(boolean val) {
        this.isUsingAntiDrops = val;
    }

    @Override
    public boolean IsUsingAntiDrops() {
        return this.isUsingAntiDrops;
    }

    @Override
    public void SetIsUsingJoinLeaveMessages(boolean val) {
        this.isUsingJoinLeaveMessages = val;
    }

    @Override
    public boolean IsUsingJoinLeaveMessages() {
        return this.isUsingJoinLeaveMessages;
    }

    @Override
    public void SetIsUsingPlayerListNames(boolean val) {
        this.isUsingPlayerListNames = val;
    }

    @Override
    public boolean IsUsingPlayerListNames() {
        return this.isUsingPlayerListNames;
    }

    @Override
    public void SetIsSkillPaneAllowed(boolean val) {
        this.isSkillPanedAllowed = val;
    }

    @Override
    public boolean IsSkillPaneAllowed() {
        return this.isSkillPanedAllowed;
    }

    @Override
    public void SetMagicWandItem(ItemStack val) {
        this.magicWandItemId = val;
    }

    @Override
    public ItemStack GetMagicWandItem() {
        return this.magicWandItemId;
    }

    @Override
    public void SetSpellbookGUILayout(HashMap<Spells.Type, String> data) {
        this.spellbookLayoutMap = data;
    }

    @Override
    public ItemStack GetSpellbookItem(Spells.Type spelltype) {
        String[] data = this.spellbookLayoutMap.get(spelltype).split(",");
        int itemid = Integer.parseInt(data[0]);
        short meta = Short.parseShort(data[1]);
        return new ItemStack(itemid, 1, meta);
    }

    @Override
    public int GetSpellbookItemSlot(Spells.Type spelltype) {
        String[] data = this.spellbookLayoutMap.get(spelltype).split(",");
        return Integer.parseInt(data[2]);
    }

    @Override
    public void SetActiveWorlds(HashMap<UUID, ParentChildWorldSet> activeWorlds) {
        this.activeWorlds = activeWorlds;
    }

    @Override
    public HashMap<UUID, ParentChildWorldSet> GetActiveWorlds() {
        return this.activeWorlds;
    }

    private void LoadGlobalConfiguration() {
        FileConfiguration c = YamlConfiguration.loadConfiguration(FileDirectory.configFile);
        HashMap<Spells.Type, String> spellbookLayoutMap = new HashMap<>();
        this.SetIsSkillPaneAllowed(c.getBoolean("Use SkillConfig Info Pane"));
        this.SetIsUsingAntiDrops(c.getBoolean("Anti-Drops Enabled"));
        this.SetIsUsingJoinLeaveMessages(c.getBoolean("Use Join/Leave Messages"));
        this.SetIsUsingPlayerListNames(c.getBoolean("Use RPGSkills PlayerListName"));
        this.SetIsUsingSkillPermissions(c.getBoolean("SkillConfig Permissions"));
        this.SetMagicWandItem(new ItemStack(c.getInt("Magic.Wand"), 1));
        for(Spells.Type spell : Spells.Type.values()) {
            spellbookLayoutMap.put(spell, c.getString("Magic.Spell GUI." + spell.toString()));
        }
        this.SetSpellbookGUILayout(spellbookLayoutMap);
    }

    private void LoadActiveWorlds() {
        FileConfiguration c = YamlConfiguration.loadConfiguration(FileDirectory.worldsConfig);
        List<String> ActiveWorldsData = (ArrayList<String>) c.getList("Active Worlds");
        for (String configWorldSet : ActiveWorldsData){
            String[] parentChildWorldData = configWorldSet.split(",");
            String childWorldName = parentChildWorldData[0];
            String parentWorldName = parentChildWorldData[1].equalsIgnoreCase("-") ? null : parentChildWorldData[1];
            ParentChildWorldSet worldSet = new ParentChildWorldSet();
            if(parentWorldName == null){ worldSet.setParentWorld(null); }
            if(childWorldName == null || childWorldName.equalsIgnoreCase("-")){continue;}//throw exception here.}
            for(World world : plugin.getServer().getWorlds()) {
                if(world.getName().equalsIgnoreCase(childWorldName)){worldSet.setChildWorld(world.getUID());}
                if(parentWorldName != null && world.getName().equalsIgnoreCase(parentWorldName)){worldSet.setParentWorld(world.getUID());}
            }
            this.activeWorlds.put(worldSet.getChildWorld(), worldSet);
        }
    }
}
