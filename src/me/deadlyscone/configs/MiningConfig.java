package me.deadlyscone.configs;

import me.deadlyscone.enums.Mining;
import me.deadlyscone.enums.SkillType;
import me.deadlyscone.enums.ToolTier;
import me.deadlyscone.interfaces.SkillConfig;
import me.deadlyscone.interfaces.skills.IMiningConfig;
import me.deadlyscone.obj.MiningBlock;
import me.deadlyscone.obj.ToolLevel;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by deadlyscone on 5/23/2016.
 */
public class MiningConfig extends SkillConfig implements IMiningConfig {

    final SkillType SKILL_TYPE = SkillType.Mining;
    private double doubleDropChance;
    private boolean isSilkSpawnersEnabled;
    private List<ToolLevel> toolLevels;
    private List<MiningBlock> miningBlocks;

    public MiningConfig(FileConfiguration configuration, UUID worldUUID) {
        super(configuration, worldUUID);
        this.toolLevels = new ArrayList<>();
        this.miningBlocks = new ArrayList<>();
        this.setToolLevels(configuration);
        this.setMiningBlocks(configuration);
        this.SetDoubleDropChance(configuration.getDouble("Double Drop Chance"));
        this.SetSilkSpawnersEnabled(configuration.getBoolean("Silk Spawners Enabled"));
    }


    private void setMiningBlocks(FileConfiguration configuration){
        for(Mining.Blocks block : Mining.Blocks.values()){
            final String blockName = block.toString();
            final String[] currentBlockData = configuration.getString("Blocks." + blockName).split(",");
            final Material blockMaterial = Material.getMaterial(blockName.toUpperCase());
            MiningBlock miningBlock = new MiningBlock();
            miningBlock.setLevelRequired(Integer.parseInt(currentBlockData[0]));
            miningBlock.setExperience(Integer.parseInt(currentBlockData[1]));
            miningBlock.setBlockMaterial(blockMaterial);
            miningBlocks.add(miningBlock);
        }
    }

    private void setToolLevels(FileConfiguration configuration){
        for(ToolTier tier : ToolTier.values()){
            final String toolLevelNodeName = "Tool Levels.";
            final String toolTierName = tier.toString();
            final int levelRequired = configuration.getInt(toolLevelNodeName + toolTierName + ".Level");
            final Material toolMaterial = Material.getMaterial((toolTierName + Mining.TOOL_SUFFIX).toUpperCase());
            ToolLevel toolLevel = new ToolLevel();
            toolLevel.setLevelRequired(levelRequired);
            toolLevel.setToolMaterial(toolMaterial);
            toolLevels.add(toolLevel);
        }
    }


    public static boolean isPickaxe(Material item) {
        switch (item){

            case WOOD_PICKAXE:
                return true;
            case STONE_PICKAXE:
                return true;
            case IRON_PICKAXE:
                return true;
            case GOLD_PICKAXE:
                return true;
            case DIAMOND_PICKAXE:
                return true;
            default:
                return false;
        }
    }

    @Override
    public SkillType GetSkillType() {
        return this.SKILL_TYPE;
    }

    @Override
    public int GetLevelRequiredToUseTool(Material tool) {
        for(ToolLevel toolLevel : this.toolLevels){
            if(toolLevel.getToolMaterial().equals(tool)){
                return toolLevel.getLevelRequired();
            }
        }
        return 0;
    }

    @Override
    public int GetLevelRequiredToMineBlock(Material block) {
        for(MiningBlock miningBlock : this.miningBlocks){
            if(miningBlock.getBlockMaterial().equals(block)){
                return miningBlock.getLevelRequired();
            }
        }
        return 0;
    }

    @Override
    public int GetExperienceForBlock(Material block) {
        for(MiningBlock miningBlock : this.miningBlocks){
            if(miningBlock.getBlockMaterial().equals(block)){
                return miningBlock.getExperience();
            }
        }
        return 0;
    }

    @Override
    public double GetDoubleDropChance() {
        return this.doubleDropChance;
    }

    @Override
    public boolean GetIsSilkSpawnersEnabled() {
        return this.isSilkSpawnersEnabled;
    }

    @Override
    public void SetDoubleDropChance(double chance) {
        this.doubleDropChance = chance;
    }

    @Override
    public void SetSilkSpawnersEnabled(boolean enabled) {
        this.isSilkSpawnersEnabled = enabled;
    }
}
