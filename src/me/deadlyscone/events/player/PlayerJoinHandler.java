package me.deadlyscone.events.player;

import me.deadlyscone.interfaces.IUserRepository;
import me.deadlyscone.main.RPGSkills;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by deadlyscone on 5/23/2016.
 */
public class PlayerJoinHandler implements Listener {

    private RPGSkills plugin;
    IUserRepository userRepository;

    public PlayerJoinHandler(RPGSkills plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
        this.userRepository = plugin.userRepository;
    }

    @EventHandler()
    public void onPlayerJoin(PlayerJoinEvent e){
        Player player = e.getPlayer();
        userRepository.CreateIfNotExists(player, plugin);
/*        player.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
        String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
        int combatLevel = getCombatLevel(worldName, player);
        String joinMessage = RPGSkills.MessageMap.get(MessageType.Join).replace("%player%", player.getName()).replace("%cblvl%", String.valueOf(combatLevel));
        if (RPGSkills.UseRPGSkillsPlayerListName){
            player.setPlayerListName(ChatColor.YELLOW + "[Level " + String.valueOf(combatLevel) + "] " + ChatColor.DARK_AQUA + player.getName());
        }
        if (RPGSkills.UseJoinLeave){
            //e.setJoinMessage(ChatColor.YELLOW + "[Level " + combatLevel + "] " + ChatColor.DARK_AQUA + player.getName() + ChatColor.GREEN + " logged on.");
            e.setJoinMessage(joinMessage);
        }*/
    }
}
