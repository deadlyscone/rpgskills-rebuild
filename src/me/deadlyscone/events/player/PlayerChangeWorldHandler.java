package me.deadlyscone.events.player;

import me.deadlyscone.main.RPGSkills;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;

/**
 * Created by deadlyscone on 5/28/2016.
 */
public class PlayerChangeWorldHandler implements Listener {


    private RPGSkills plugin;
    public PlayerChangeWorldHandler(RPGSkills plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerChangeWorld(PlayerChangedWorldEvent e){
        plugin.userRepository.CreateIfNotExists(e.getPlayer(), plugin);
    }
}
