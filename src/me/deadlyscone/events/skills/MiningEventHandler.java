package me.deadlyscone.events.skills;

import me.deadlyscone.enums.Mining;
import me.deadlyscone.enums.Permissions;
import me.deadlyscone.enums.SkillType;
import me.deadlyscone.interfaces.*;
import me.deadlyscone.interfaces.skills.IMiningConfig;
import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.configs.MiningConfig;
import me.deadlyscone.utils.Console;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;
import java.util.UUID;

/**
 * Created by deadlyscone on 5/23/2016.
 */
public class MiningEventHandler extends SkillEvent implements Listener {

    public final SkillType THIS_SKILL = SkillType.Mining;

    public MiningEventHandler( RPGSkills plugin){
        super(plugin);
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent e) {
        Player player = e.getPlayer();
        final UUID playerUUID = player.getUniqueId();
        final UUID worldUUID = player.getWorld().getUID();
        final IMiningConfig config = this.getSkillConfigContainer().getMiningConfig(worldUUID);
        final boolean isUsingSkillPerms = false; // TODO: implement method to get this value
        IUsers user = this.userRepository.GetUser(playerUUID);
        Block block = e.getBlock();
        //final boolean isUsingSkillPerms = repositoryContainer.ConfigurationRepository().IsUsingSkillPermissions();

        if (config != null && config.IsEnabled()
                && (player.hasPermission(Permissions.RPGSKILLS_SKILLS_MINING.toString()) ? true : isUsingSkillPerms)
                && player.getGameMode() != GameMode.CREATIVE && !e.isCancelled()){
            int currentLevel = user.GetCurrentSkillLevel(config);
            Material materialHand = player.getItemInHand().getType();
            Material materialBlock = e.getBlock().getType();
            final boolean isPickaxe = MiningConfig.isPickaxe(materialHand);
            final int levelRequiredForPickaxe = config.GetLevelRequiredToUseTool(materialHand);
            final boolean isMiningBlock = Mining.Blocks.isMiningBlock(materialBlock);

            if (isPickaxe && e.isCancelled() == false && currentLevel >= levelRequiredForPickaxe && isMiningBlock){
                final int levelRequiredForBlock = config.GetLevelRequiredToMineBlock(materialBlock);
                final int experienceForBlock = config.GetExperienceForBlock(materialBlock);
                final boolean isBreakingPlacedBlock = this.userRepository.isBlockPlacedByPlayer(block.getLocation());
                if(currentLevel >= levelRequiredForBlock){
                    if (!isBreakingPlacedBlock){
                        checkDoubleDrop(player, currentLevel, block, block.getDrops().size());
                        user.GiveSkillExperience(worldUUID, THIS_SKILL, experienceForBlock);
                        Console.log(user.GetSkillExperience(worldUUID, THIS_SKILL));
                        user.CheckIfPlayerLeveled(config, player, currentLevel);
                    } else if (isBreakingPlacedBlock) {
                        this.userRepository.RemovePlayerBlockPlaceLocation(block.getLocation());
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(levelRequiredForBlock) + " to mine this block.");
                    e.setCancelled(true);
                }
            } else if (isPickaxe && isMiningBlock && e.isCancelled() == false){
                player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(levelRequiredForPickaxe) + " to use this pickaxe.");
                e.setCancelled(true);
            }
        }
    }



    @SuppressWarnings("deprecation")
    private void checkDoubleDrop(Player player, int currentLevel, Block block, int amount) {
        Random r = new Random();
        final IMiningConfig config = this.getSkillConfigContainer().getMiningConfig(player.getWorld().getUID());
        final double playerDoubleDropChance = config.GetDoubleDropChance();
        final boolean isMiningBlock = Mining.Blocks.isMiningBlock(block.getType());
        double ddc = playerDoubleDropChance * currentLevel;
        double chance = r.nextDouble()*100;
        ItemStack itemToDrop = (player.getItemInHand().containsEnchantment(Enchantment.SILK_TOUCH) && isMiningBlock) ?
                new ItemStack(block.getType(), 1, block.getData()) : block.getDrops().toArray(new ItemStack[block.getDrops().size()])[0];
        if (chance <= ddc){
            block.getWorld().dropItemNaturally(block.getLocation(), itemToDrop);
        }
    }
}
