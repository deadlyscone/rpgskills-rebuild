package me.deadlyscone.obj;

import me.deadlyscone.enums.SkillType;
import me.deadlyscone.interfaces.ISkillConfig;
import me.deadlyscone.interfaces.IUsers;
import me.deadlyscone.interfaces.skills.IMiningConfig;
import me.deadlyscone.utils.Console;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.awt.geom.QuadCurve2D;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collector;

/**
 * Created by deadlyscone on 11/30/2015.
 */

public class Users implements IUsers, Serializable {

    private List<ExperienceStore> playerExperienceStore;
    private UUID playerUUID = null;
    private String playerDisplayName = null;
    private boolean scoreboardEnabled = false;
    private int playerMP = 0;


    public Users(UUID playerUUID, String playerDisplayName){
        this.playerUUID = playerUUID;
        this.playerDisplayName = playerDisplayName;
        this.playerExperienceStore = new ArrayList<>();
    }

    public double GetSkillExperience(UUID worldUUID, SkillType skillType){
        boolean isStoreEmpty = this.playerExperienceStore.size() == 0;
        boolean storeContainsSkill = this.hasSkillEntry(worldUUID, skillType);
        if(isStoreEmpty || !storeContainsSkill){ this.AddSkill(worldUUID, skillType); }
        return this.playerExperienceStore.stream()
                .filter(x -> x.getWorldUUID().equals(worldUUID) && x.getSkillType().equals(skillType))
                .mapToDouble(d -> d.getPlayerExperience()).findFirst().orElse(0);
    }

    private boolean hasSkillEntry(UUID worldUUID, SkillType skillType) {
        return this.playerExperienceStore.stream().filter(x -> x.getWorldUUID().equals(worldUUID) && x.getSkillType().equals(skillType)).count() > 0;
    }

    @Override
    public UUID GetPlayerUUID() {
        return this.playerUUID;
    }

    @Override
    public String GetPlayerDisplayName() {
        return this.playerDisplayName;
    }

    @Override
    public boolean GetScoreboardEnabled() {
        return this.scoreboardEnabled;
    }

    @Override
    public int GetCurrentSkillLevel(ISkillConfig skillConfig) {
        double currentPlayerExperience = this.GetSkillExperience(skillConfig.GetWorldUUID(), skillConfig.GetSkillType());
        int startingLevel = skillConfig.GetStartingLevel();
        int maxLevel = skillConfig.GetMaxLevel();
        double maxLevelExperience = skillConfig.GetMaxLevelExperience();

        if(currentPlayerExperience >= maxLevelExperience){
            return maxLevel;
        }else if(currentPlayerExperience != maxLevelExperience && currentPlayerExperience < maxLevelExperience){
            for(int level = startingLevel; level < (maxLevel + 1); level++){
                if (currentPlayerExperience <= skillConfig.GetExperienceForLevel(level)){
                    return level;
                }
            }
        }
        return startingLevel;
    }

    @Override
    public void CheckIfPlayerLeveled(final ISkillConfig config, final Player player, final int oldLevel){
        int currentLevel = this.GetCurrentSkillLevel(config);
        if(currentLevel > oldLevel) {
            player.sendMessage("Your mining level is now " + currentLevel);
            player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LARGE_BLAST_FAR, 1f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 0.1f, 0.5f);
            //update the level above the damagers head
            //JoinLeaveHandler.updatePlayerPrefix(player);
            Console.log("test");
        }
    }

    @Override
    public int GetCurrentCombatLevel(UUID worldUUID) {
        return 0;
    }

    @Override
    public int GetCurrentMP(UUID worldUUID) {
        return this.playerMP;
    }

    @Override
    public void SetCurrentMP(UUID worldUUID, int amount) {
        this.playerMP = amount;
    }

    @Override
    public void AddMP(UUID worldUUID, int amount) {
        this.playerMP += amount;
    }

    @Override
    public void TakeMP(UUID worldUUID, int amount) {

        this.playerMP -= amount;
    }

    @Override
    public void SetSkillExperience(UUID worldUUID, SkillType skillType, double experience) {
        this.playerExperienceStore.stream()
                .filter(store -> store.getWorldUUID().equals(worldUUID) && store.getSkillType().equals(skillType))
                .forEach(store -> store.setPlayerExperience(experience));
        //TODO: check if player leveled up.
    }

    @Override
    public void GiveSkillExperience(UUID worldUUID, SkillType skillType, double addedExperience) {
        double currentExperience = this.GetSkillExperience(worldUUID, skillType);
        this.SetSkillExperience(worldUUID, skillType, (currentExperience + addedExperience) );
    }

    @Override
    public void TakeSkillExperience(UUID worldUUID, SkillType skillType, double takenExperience) {
        double currentExperience = this.GetSkillExperience(worldUUID, skillType);
        if( (currentExperience - takenExperience) < 0.00d ) {
            this.SetSkillExperience(worldUUID, skillType, 0.00d );
        }else{
            this.SetSkillExperience(worldUUID, skillType, (currentExperience - takenExperience) );
        }
    }

    private void AddSkill(UUID worldUUID, SkillType skillType){
        ExperienceStore experienceStore = new ExperienceStore(worldUUID);
        experienceStore.setSkillType(skillType);
        experienceStore.setPlayerExperience(0);
        this.playerExperienceStore.add(experienceStore);
    }

    /*private void AddSkillEntry(ISkillConfig skill){
        Iterator iterator = this.experienceStoreModelStore.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<SkillType, Double> pair = (Map.Entry<SkillType, Double>)iterator.next();
            if(!pair.getKey().equals(skill.GetSkillType())){ continue; }
            Double currentExperience = pair.getValue();
            if(currentExperience == null || skill.GetStartingLevelExperience() > currentExperience){
                pair.setValue(skill.GetStartingLevelExperience());
            }
        }
    }*/
}
