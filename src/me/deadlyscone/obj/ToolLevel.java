package me.deadlyscone.obj;

import org.bukkit.Material;

/**
 * Created by Austin on 10/23/2016.
 */
public class ToolLevel {

    private int levelRequired;
    private Material toolMaterial;

    public int getLevelRequired() {
        return levelRequired;
    }

    public void setLevelRequired(int levelRequired) {
        this.levelRequired = levelRequired;
    }

    public Material getToolMaterial() {
        return toolMaterial;
    }

    public void setToolMaterial(Material toolMaterial) {
        this.toolMaterial = toolMaterial;
    }
}
