package me.deadlyscone.obj;

import java.util.UUID;

/**
 * Created by Austin on 10/21/2016.
 */
public class ParentChildWorldSet {

    private UUID childWorld;
    private UUID parentWorld;

    public ParentChildWorldSet(UUID parentWorld, UUID childWorld){
        this.setChildWorld(childWorld);
        this.setParentWorld(parentWorld);
    }

    public ParentChildWorldSet(){}


    public UUID getChildWorld() {
        return childWorld;
    }

    public void setChildWorld(UUID childWorld) {
        this.childWorld = childWorld;
    }

    public UUID getParentWorld() {
        return parentWorld;
    }

    public void setParentWorld(UUID parentWorld) {
        this.parentWorld = parentWorld;
    }

    public boolean hasParentWorld(){
        return this.getParentWorld() != null;
    }

}
