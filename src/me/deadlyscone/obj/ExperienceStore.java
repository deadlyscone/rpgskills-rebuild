package me.deadlyscone.obj;

import me.deadlyscone.enums.SkillType;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Austin on 10/26/2016.
 */
public class ExperienceStore implements Serializable {

    private double playerExperience;
    private SkillType skillType;
    private UUID worldUUID;
    private int lastRecordedLevel;


    public int getLastRecordedLevel() {
        return lastRecordedLevel;
    }

    public void setLastRecordedLevel(int lastRecordedLevel) {
        this.lastRecordedLevel = lastRecordedLevel;
    }

    public double getPlayerExperience() {
        return playerExperience;
    }

    public void setPlayerExperience(double playerExperience) {
        this.playerExperience = playerExperience;
    }

    public SkillType getSkillType() {
        return skillType;
    }

    public void setSkillType(SkillType skillType) {
        this.skillType = skillType;
    }

    public UUID getWorldUUID(){return this.worldUUID;}

    public ExperienceStore(UUID worldUUID){
        this.worldUUID = worldUUID;
    }

}
