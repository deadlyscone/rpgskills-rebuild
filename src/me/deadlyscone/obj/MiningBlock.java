package me.deadlyscone.obj;

import org.bukkit.Material;

/**
 * Created by Austin on 10/23/2016.
 */
public class MiningBlock {

    private int levelRequired;

    public int getLevelRequired() {
        return levelRequired;
    }

    public void setLevelRequired(int levelRequired) {
        this.levelRequired = levelRequired;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public Material getBlockMaterial() {
        return blockMaterial;
    }

    public void setBlockMaterial(Material blockMaterial) {
        this.blockMaterial = blockMaterial;
    }

    private int experience;
    private Material blockMaterial;
}
