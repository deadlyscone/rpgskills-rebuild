package me.deadlyscone.repositories;

import com.mysql.fabric.xmlrpc.base.Array;
import me.deadlyscone.external.NameFetcher;
import me.deadlyscone.interfaces.IUserRepository;
import me.deadlyscone.interfaces.IUsers;
import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.obj.Users;
import me.deadlyscone.utils.Console;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Created by deadlyscone on 5/28/2016.
 */

public class UserRepository implements IUserRepository {

    //////////////////////////////////////////////////////////////////////////////////
    /** Private Fields **/
    //////////////////////////////////////////////////////////////////////////////////
    private List<IUsers> userList = new ArrayList<IUsers>();
    private ArrayList<String> playerPlacedBlocks = new ArrayList<>();

    //////////////////////////////////////////////////////////////////////////////////
    /** Constructors **/
    //////////////////////////////////////////////////////////////////////////////////
    public UserRepository(){
        this.userList = new ArrayList<>();
    }

    public UserRepository(List<IUsers> userList){
        this.userList = userList;
    }

    //////////////////////////////////////////////////////////////////////////////////
    /** Public Methods **/
    //////////////////////////////////////////////////////////////////////////////////

    @Override
    public String LookupPlayerName(UUID uuid){
        List<UUID> uuids = new ArrayList<UUID>();
        uuids.add(uuid);
        Map<UUID, String> matched = new HashMap<UUID, String>();
        try {
            matched = new NameFetcher(uuids).call();
        } catch (Exception e) {
            Console.severe(e.getMessage());
        }
        return matched.get(uuid);
    }

    @Override
    public IUsers GetUser(UUID uuid){
        IUsers rtnUser = null;
        for(IUsers user : this.userList){
            if(user.GetPlayerUUID().equals(uuid)){
                rtnUser = user;
            }
        }
        return rtnUser;
    }

    /** User Creation Methods **/
    @Override
    public boolean UserExists(IUsers user){
        return GetUser(user.GetPlayerUUID()) != null;
    }
    @Override
    public boolean UserExists(Player player){
        return GetUser(player.getUniqueId()) != null;
    }
    @Override
    public void CreateIfNotExists(Player player, RPGSkills plugin){
        if(!UserExists(player)){
            AddUser(new Users(player.getUniqueId(), player.getDisplayName()));
            Console.warning("Welcome newcomer. SIZE > " + this.userList.size());
        }else {
            Console.warning("You have been here before i see.");
        }
    }
    @Override
    public void UpdateUser(IUsers user){
        UUID uuid = user.GetPlayerUUID();
        for(int i = 0; i < this.userList.size(); i++){
            UUID currentUUID = userList.get(i).GetPlayerUUID();
            if(currentUUID.equals(uuid)) {
                userList.set(i, user);
                break;
            }
        }
    }
    @Override
    public void AddUser(IUsers user){
        if(GetUser(user.GetPlayerUUID()) == null){
            Console.log("Adding " + user.GetPlayerUUID() + " to the user repository.");
            this.userList.add(user);
        }
    }
    @Override
    public void AddOrUpdateUser(IUsers user){
        if(this.GetUser(user.GetPlayerUUID()) == null){
            AddUser(user);
        }else{
            UpdateUser(user);
        }
    }
    @Override
    public boolean isBlockPlacedByPlayer(Location location){
        boolean isPlacedByPlayer = false;
        for(int i = 0; i<this.playerPlacedBlocks.size(); i++) {
            String serializedLocation = this.SerializeLocation(location);
            if(serializedLocation.equals(playerPlacedBlocks.get(i))) {
                isPlacedByPlayer = true;
            }
        }
        return isPlacedByPlayer;
    }
    @Override
    public void AddPlayerBlockPlaceLocation(Material material, Location location) {
        if(isMaterialOfConcern(material) && !isBlockPlacedByPlayer(location)) {
            this.playerPlacedBlocks.add(SerializeLocation(location));
        }
    }
    @Override
    public void RemovePlayerBlockPlaceLocation(Location location) {
        for(int i = 0; i < this.playerPlacedBlocks.size(); i++) {
            String serializedLocation = SerializeLocation(location);
            if(serializedLocation.equals(this.playerPlacedBlocks.get(i))){
                this.playerPlacedBlocks.remove(i);
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////
    /** Private Methods **/
    //////////////////////////////////////////////////////////////////////////////////

    private boolean isMaterialOfConcern(Material material) {
        boolean rtnBoolean = false;
        switch (material.ordinal()) {
            case 1://STONE
            case 87://NETHERRACK
            case 153://QUARTZ_ORE
            case 16://COAL_ORE
            case 15://IRON_ORE
            case 14://GOLD_ORE
            case 73://REDSTONE_ORE
            case 74://GLOWING_REDSTONE_ORE
            case 21://LAPIS_ORE
            case 129://EMERALD_ORE
            case 56://DIAMOND_ORE
            case 12://SAND
            case 2://GRASS
            case 3://DIRT
            case 13://GRAVEL
            case 60://SOIL
            case 88://SOUL_SAND
            case 82://CLAY
            case 110://MYCEL
            case 17://LOG
            case 162://LOG2
            case 103:
            case 86://PUMPKIN
            case 83://SUGAR_CANE_BLOCK
                rtnBoolean = true;
            default:
                break;
        }
        return rtnBoolean;
    }
    private Location DeserializeLocation(RPGSkills plugin, String location) {
        String[] data = location.split(":");
        World world = plugin.getServer().getWorld(UUID.fromString(data[3]));
        return new Location(world, Double.parseDouble(data[0]), Double.parseDouble(data[1]), Double.parseDouble(data[2]));
    }

    private String SerializeLocation(Location location) {
        String x = String.valueOf(location.getBlockX());
        String y = String.valueOf(location.getBlockY());
        String z = String.valueOf(location.getBlockZ());
        UUID world = location.getWorld().getUID();
        String finalCoords = x + ":" + y + ":" + z + ":" + world;
        return finalCoords;
    }
}
