package me.deadlyscone.interfaces.skills;

import me.deadlyscone.interfaces.ISkillConfig;
import org.bukkit.Material;

/**
 * Created by austi on 10/21/2016.
 */
public interface IMiningConfig extends ISkillConfig {
    int GetLevelRequiredToUseTool(Material tool);
    int GetLevelRequiredToMineBlock(Material block);
    int GetExperienceForBlock(Material block);
    double GetDoubleDropChance();
    boolean GetIsSilkSpawnersEnabled();

    void SetDoubleDropChance(double chance);
    void SetSilkSpawnersEnabled(boolean enabled);
}
