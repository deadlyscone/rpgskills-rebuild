package me.deadlyscone.interfaces;

import me.deadlyscone.enums.SkillType;
import me.deadlyscone.interfaces.skills.IMiningConfig;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by deadlyscone on 5/23/2016.
 */
public interface IUsers {
    void SetSkillExperience(UUID worldUUID, SkillType skillType, double experience);
    void GiveSkillExperience(UUID worldUUID, SkillType skillType, double addedExperience);
    void TakeSkillExperience(UUID worldUUID, SkillType skillType, double takenExperience);
    double GetSkillExperience(UUID worldUUID, SkillType skillType);
    UUID GetPlayerUUID();
    String GetPlayerDisplayName();
    boolean GetScoreboardEnabled();
    int GetCurrentSkillLevel(ISkillConfig skillConfig);
    int GetCurrentCombatLevel(UUID worldUUID);
    int GetCurrentMP(UUID worldUUID);
    void SetCurrentMP(UUID worldUUID, int amount);
    void AddMP(UUID worldUUID, int amount);
    void TakeMP(UUID worldUUID, int amount );
    void CheckIfPlayerLeveled(final ISkillConfig config, final Player player, final int oldLevel);
}
