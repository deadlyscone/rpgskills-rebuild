package me.deadlyscone.interfaces;

import me.deadlyscone.enums.SkillType;
import org.bukkit.World;
import org.bukkit.event.Listener;
import org.omg.CosNaming.IstringHelper;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by deadlyscone on 5/23/2016.
 */
public interface ISkillConfig {

    SkillType GetSkillType();
    int GetMaxLevel();
    int GetStartingLevel();
    double GetMaxLevelExperience();
    double GetStartingLevelExperience();
    HashMap<Integer, Double> GetExperiencePerLevel();
    UUID GetWorldUUID();
    double GetExperienceForLevel(int level);
    boolean IsEnabled();
    void SetIsEnabled(boolean enabled);
    void SetMaxLevel(int level);
    double GetGainFactor();
    void SetGainFactor(double factor);
}

