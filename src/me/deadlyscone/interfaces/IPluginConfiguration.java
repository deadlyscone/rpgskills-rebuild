package me.deadlyscone.interfaces;

import me.deadlyscone.enums.Spells;
import me.deadlyscone.obj.ParentChildWorldSet;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by deadlyscone on 5/29/2016.
 */
public interface IPluginConfiguration extends IRepository {

    void SetIsUsingSkillPermissions(boolean val);
    boolean IsUsingSkillPermissions();

    void SetIsUsingAntiDrops(boolean val);
    boolean IsUsingAntiDrops();

    void SetIsUsingJoinLeaveMessages(boolean val);
    boolean IsUsingJoinLeaveMessages();

    void SetIsUsingPlayerListNames(boolean val);
    boolean IsUsingPlayerListNames();

    void SetIsSkillPaneAllowed(boolean val);
    boolean IsSkillPaneAllowed();

    void SetMagicWandItem(ItemStack val);
    ItemStack GetMagicWandItem();

    void SetSpellbookGUILayout(HashMap<Spells.Type, String> data);
    ItemStack GetSpellbookItem(Spells.Type spelltype);
    int GetSpellbookItemSlot(Spells.Type spelltype);

    void SetActiveWorlds(HashMap<UUID, ParentChildWorldSet> activeWorlds);
    HashMap<UUID, ParentChildWorldSet> GetActiveWorlds();
}
