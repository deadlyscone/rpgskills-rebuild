package me.deadlyscone.interfaces;

import me.deadlyscone.main.RPGSkills;

/**
 * Created by deadlyscone on 5/23/2016.
 */
public abstract class SkillEvent {
    private RPGSkills plugin;
    protected IUserRepository userRepository;
    private ISkillConfigContainer _skillConfigContainer;

    public SkillEvent(RPGSkills plugin){
        this.plugin = plugin;
        this._skillConfigContainer = plugin.skillConfigContainer;
        this.userRepository = plugin.userRepository;
    }

    protected RPGSkills getPlugin(){
        return this.plugin;
    }

    protected ISkillConfigContainer getSkillConfigContainer(){
        return  this._skillConfigContainer;
    }
}
