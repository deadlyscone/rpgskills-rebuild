package me.deadlyscone.interfaces;

import me.deadlyscone.interfaces.skills.IMiningConfig;

import java.util.UUID;

/**
 * Created by Austin on 10/23/2016.
 */
public interface ISkillConfigContainer {
    void addMiningConfig(UUID worldUUID, IMiningConfig config);
    IMiningConfig getMiningConfig(UUID worldUUID);
}
