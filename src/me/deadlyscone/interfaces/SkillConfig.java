package me.deadlyscone.interfaces;

import me.deadlyscone.enums.SkillType;
import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.utils.Console;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.UUID;

public abstract class SkillConfig implements ISkillConfig {
    protected UUID _worldUUID;
    protected boolean _isEnabled;
    protected double _gainFactor;
    protected int _maxLevel;
    protected int _startingLevel;
    protected HashMap<Integer, Double> _skillExperiencePerLevel;

    public SkillConfig(FileConfiguration configuration, UUID worldUUID){
        if( configuration == null || configuration.getValues(false) == null){ RPGSkills.halt(new Exception("ErrorID: 0x00000000")); }
        this._worldUUID = worldUUID;
        this._skillExperiencePerLevel = new HashMap<>();
        this._gainFactor = configuration.getDouble("Gain Factor");
        this._isEnabled = configuration.getBoolean("Enabled");
        this._maxLevel = configuration.getInt("Level Cap");
        this._startingLevel = configuration.getInt("Starting Level");
        this.CalculateExperiencePerLevel();
    }


    @Override
    public UUID GetWorldUUID(){
        return this._worldUUID;
    }

    @Override
    public double GetExperienceForLevel(int level) {
        if(level < this._startingLevel || level > this._maxLevel){ Console.severe("Skill level ("+level+") requested is out of bounds. E0001-00"); }
        return this._skillExperiencePerLevel.get(level);
    }

    @Override
    public boolean IsEnabled() {
        return this._isEnabled;
    }

    @Override
    public void SetIsEnabled(boolean enabled) {
        this._isEnabled = enabled;
    }

    @Override
    public void SetMaxLevel(int level) {
        this._maxLevel = level;
    }

    @Override
    public double GetGainFactor() {
        return this._gainFactor;
    }

    @Override
    public void SetGainFactor(double factor) {
        this._gainFactor = factor;
    }

    @Override
    public abstract SkillType GetSkillType();

    @Override
    public int GetMaxLevel() {
        return this._maxLevel;
    }

    @Override
    public int GetStartingLevel() {
        return this._startingLevel;
    }

    @Override
    public double GetMaxLevelExperience() {
        return this._skillExperiencePerLevel.get(this._maxLevel);
    }

    @Override
    public double GetStartingLevelExperience() {
        return this._skillExperiencePerLevel.get(this._startingLevel);
    }

    @Override
    public HashMap<Integer, Double> GetExperiencePerLevel() {
        return this._skillExperiencePerLevel;
    }

    private void CalculateExperiencePerLevel(){
        for (int i = this._startingLevel; i < this._maxLevel + 1; i++){
            double factor = this._gainFactor;
            double nextLevel = i + 1;
            double expReqforLevel = i* Math.round((Math.pow(nextLevel, factor)));
            this._skillExperiencePerLevel.put(i, expReqforLevel);
        }
    }
}
