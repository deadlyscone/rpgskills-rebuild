package me.deadlyscone.interfaces;

import me.deadlyscone.main.RPGSkills;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by deadlyscone on 5/28/2016.
 */
public interface IUserRepository extends IRepository {
    IUsers GetUser(UUID uuid);
    String LookupPlayerName(UUID uuid);
    boolean UserExists(IUsers user);
    boolean UserExists(Player player);
    void CreateIfNotExists(Player player, RPGSkills plugin);
    void UpdateUser(IUsers user);
    void AddUser(IUsers user);
    void AddOrUpdateUser(IUsers user);
    boolean isBlockPlacedByPlayer(Location location);
    void AddPlayerBlockPlaceLocation(Material material, Location location);
    void RemovePlayerBlockPlaceLocation(Location location);
}
